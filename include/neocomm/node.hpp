/*
 * Copyright (C) 2018 Ortega Froysa, Nicolás <nortega@themusicinnoise.net>
 * Author: Ortega Froysa, Nicolás <nortega@themusicinnoise.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "neocomm/channel.hpp"

#include <opendht.h>
#include <vector>

namespace neocomm {

/**
 * @brief Node object of the network model.
 */
class node {
public:
	/**
	 * @brief Initalize the node.
	 *
	 * @param port Local port to bind to.
	 * @param alias Set the alias of the user.
	 */
	node(const unsigned short port = 31133,
			const std::string &alias = "unknown");
	~node();

	/**
	 * @brief Bootstrap to a given node within the network.
	 *
	 * @param address DNS/IP of the node.
	 * @param port Port of the node.
	 */
	inline void connect(const std::string &address,
			const std::string &port) {
		network.bootstrap(address, port);
	}
	/**
	 * @brief Import a list of nodes to connect to.
	 *
	 * @param file_path The path to the node file.
	 */
	void import_nodes(const std::string &file_path); // TODO
	/**
	 * @brief Export the list of nodes to which our node is currently
	 * connected to into a file.
	 *
	 * @param file_path Path to the node file.
	 */
	void export_nodes(const std::string &file_path); // TODO

	/**
	 * @brief Join a channel.
	 *
	 * @param name Name of the channel.
	 *
	 * @note If the channel is already joined on this node
	 * then it does nothing and returns a nullptr.
	 *
	 * @return Returns a pointer to the channel object.
	 */
	channel *join_channel(const std::string &name);
	/**
	 * @brief Get a pointer to a channel object by name.
	 *
	 * @param name The name of the channel.
	 *
	 * @note If the channel has not been joined on this node
	 * then it returns nullptr.
	 *
	 * @return A pointer to the channel object.
	 */
	channel *get_channel(const std::string &name);
	/**
	 * @brief Leave a channel by name.
	 *
	 * @note If the channel has not been joined on this node
	 * it does nothing.
	 *
	 * @param name The name of the channel.
	 */
	void leave_channel(const std::string &name);
	/**
	 * @brief Leave a channel by pointer to the object.
	 *
	 * @note If the channel has not been joined on this node
	 * it does nothing.
	 *
	 * @param chan The channel object.
	 */
	inline void leave_channel(channel *chan) {
		leave_channel(chan->get_name());
	}

	/**
	 * @brief Get the current alias for the user.
	 *
	 * @return The current alias of the user.
	 */
	inline std::string get_alias() const {
		return alias;
	}
	/**
	 * @brief Set the alias of the user.
	 *
	 * @param alias The new alias of the user.
	 */
	inline void set_alias(const std::string &alias) {
		this->alias = alias;
	}

private:
	std::string alias;
	dht::DhtRunner network;
	std::vector<channel*> channels;
};

}
