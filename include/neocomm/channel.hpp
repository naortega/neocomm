/*
 * Copyright (C) 2018 Ortega Froysa, Nicolás <nortega@themusicinnoise.net>
 * Author: Ortega Froysa, Nicolás <nortega@themusicinnoise.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "neocomm/message.hpp"

#include <string>
#include <list>
#include <opendht.h>

namespace neocomm {

/**
 * @brief Represents the channel of a given node. Can be used
 * to send and receive messages.
 */
class channel {
public:
	/**
	 * @brief Initialize a channel object (this should pretty
	 * much only be done by the node object).
	 *
	 * @param name Name of the channel.
	 * @param network A pointer to the OpenDHT node.
	 */
	channel(const std::string &name, dht::DhtRunner *network);
	~channel();

	/**
	 * @brief Get the name of the channel.
	 *
	 * @return The name of the channel.
	 */
	inline std::string get_name() const {
		return name;
	}

private:
	const std::string name;
	const dht::InfoHash key;
	dht::DhtRunner *network;
	std::future<size_t> token;
	std::list<struct message> messages;
};
}
