===============
*** NeoComm ***
===============
A decentralized chat room network aiming to work against censorship. The
objective is to create a secure form of chat room communication similar to that
of IRC, but decentralized in order to remove any kind of power structure or
model, attempting to prevent censorship of the network by outside forces as
well as censorship within the network itself.

NeoComm is a library and not an application unto itself, and is meant to be
incorporated into applications to take advantage of its decentralized nature.
Feel free to use it in your own software.

# Network Model
---------------
Unlike a federated network model, NeoComm does not require a master server or
reliance on the benevolence of a few slave servers, but rather uses a
decentralized P2P network model using Distributed Hash Tables[0], and more
specifically the OpenDHT library[1]. With this model each client serves as a
node in the network that can be used to relay information, and those that
forward ports can even be used as entry nodes to the network itself, therefore
removing reliance on servers hosted by particular individuals (including
myself).

This also means that the larger the network gets, the more resistant it will
become and the easier it will be for users to bypass censorship.

[0] https://en.wikipedia.org/wiki/Distributed_hash_table
[1] https://github.com/savoirfairelinux/opendht

# API Documentation
-------------------
For documentation, NeoComm uses Doxygen[0], which generates easy to read HTML
documentation from the comments within the header files of the project. If you
install Doxygen you can generate the documentation by running the `doxygen'
command in the root directory of the project.

Public documentation will be posted every major version.

[0] http://www.stack.nl/~dimitri/doxygen/

# Clients
---------
There are currently no clients for NeoComm. If you would like to add one to
this list then contact <nortega@themusicinnoise.net> with relevant information.

# Compiling
-----------
To build the project you will require the following dependencies:

 - A C++11 compatible compiler (GNU GCC is recommended)
 - OpenDHT >=v1.4.0 (https://github.com/savoirfairelinux/opendht)
 - msgpack-c (https://github.com/msgpack/msgpack-c)
 - CMake (https://cmake.org/)

You can then run the following commands to compile the project:

  cd build/
  cmake ..
  make

This will create a static release binary of the project. If you wish to compile
with debug options then you can append the `-DCMAKE_BUILD_TYPE=debug' flag to
the `cmake' command. Likewise, to create a dynamic binary use the
`-DBUILD_SHARED_LIB=ON' flag.

For installation or packaging you will want to use the
`-DCMAKE_INSTALL_PREFIX=<install_dir>' option. If you are installing it
manually then you should probably use `/usr/local/' as the installation
directory. Otherwise, if you are packaging, please consult your distribution's
policies.

# Contributing
--------------
NeoComm is free/libre software[0]. Before contributing any code, please look at
the `CONTRIBUTING' file for contribution policy. You can send patch files with
contributions to <nortega@themusicinnoise.net> or to the community via
Bitmessage (see the 'Community' section below).

[0] https://www.gnu.org/philosophy/free-sw.html

# Community
-----------
NeoComm uses Bitmessage[0] for community collaboration. To receive updates on
NeoComm subscribe to the following address:

BM-NBHAvmbVbUnGxLDmGhZQ66uWUijd7uGi

There is also a public broadcast channel available:
Chan name/passphrase: neocomm
Chan address: BM-2cTDst8Xf8afFXFNdtT3qhU4vAJ4nXTYUW

[0] https://bitmessage.org/wiki/Main_Page

# License
---------
This project is licensed under the terms & conditions of the GNU Affero General
Public License version 3 or greater (see `LICENSE' file for more information).
