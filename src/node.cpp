/*
 * Copyright (C) 2018 Ortega Froysa, Nicolás <nortega@themusicinnoise.net>
 * Author: Ortega Froysa, Nicolás <nortega@themusicinnoise.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "neocomm/node.hpp"
#include "neocomm/message.hpp"

using namespace neocomm;

node::node(unsigned short port, const std::string &alias) :
		alias(alias) {
	// TODO: see about preserving an identity
	network.run(port, dht::crypto::generateIdentity(), true);
}

node::~node() {
	network.join();
}

channel *node::join_channel(const std::string &name) {
	// don't do anything if already joined
	if(get_channel(name))
		return nullptr;

	channels.push_back(new channel(name, &network));
	return channels.back();
}

channel *node::get_channel(const std::string &name) {
	for(auto i : channels)
	{
		if(i->get_name() == name)
			return i;
	}
	return nullptr;
}

void node::leave_channel(const std::string &name) {
	for(auto i = channels.begin();
			i not_eq channels.end(); ++i)
	{
		if((*i)->get_name() == name)
		{
			delete *i;
			channels.erase(i);
			return;
		}
	}
}
