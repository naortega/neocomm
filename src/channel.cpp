/*
 * Copyright (C) 2018 Ortega Froysa, Nicolás <nortega@themusicinnoise.net>
 * Author: Ortega Froysa, Nicolás <nortega@themusicinnoise.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "neocomm/channel.hpp"

using namespace neocomm;

channel::channel(const std::string &name,
		dht::DhtRunner *network) :
		name(name), key(dht::InfoHash::get(name)),
		network(network) {
	token = network->listen<struct message>(key,
			[&](struct message &&msg) {
				messages.push_back(msg);
				return true;
			});
}

channel::~channel() {
	network->cancelListen(key, token.get());
}
